function countLetter(letter, sentence) {
    let result = 0;

    // Check first whether the letter is a single character.
    
    if (letter.length === 1) {
    
    // If letter is a single character,  count how many times a letter has occurred in a given sentence then return count.

    for (let count = 0; count < sentence.length; count++) {
        if (sentence[count] === letter) {
            result++;
        }
    }

    return result;

    } else {

    // If letter is invalid, return undefined.

    return undefined;

    }
}



function isIsogram(text) {
    // An isogram is a word where there are no repeating letters.
    // The function should disregard text casing before doing anything else.

    const lowerText = text.toLowerCase();
    const lettersFound = {};

    for (let textCount = 0; textCount < lowerText.length; textCount++) {

    // If the function finds a repeating letter, return false. Otherwise, return true.

        if (lettersFound[lowerText[textCount]]) {
            return false;

        } else {
            lettersFound[lowerText[textCount]] = true;
        }
    }

    return true;    
};



function purchase(age, price) {
    // Return undefined for people aged below 13.

    if (age < 13) {
        return undefined;
    }

    // Return the discounted price (rounded off) for students aged 13 to 21 and senior citizens. (20% discount)

    else if (age >= 13 && age <= 21 || age >= 65) {

        const discountedPrice = Math.round(price * 0.8);
        return discountedPrice.toString();

    } else {


    // Return the rounded off price for people aged 22 to 64.

        const roundedPrice = Math.round(price);
        return roundedPrice.toString();

    }


};
    // The returned value should be a string.
    


let items = [
    { id: 'tltry001', name: 'soap', stocks: 14, category: 'toiletries' },
    { id: 'tltry002', name: 'shampoo', stocks: 8, category: 'toiletries' },
    { id: 'tltry003', name: 'tissues', stocks: 0, category: 'toiletries' },
    { id: 'gdgt001', name: 'phone', stocks: 0, category: 'gadgets' },
    { id: 'gdgt002', name: 'monitor', stocks: 0, category: 'gadgets' }
];

function findHotCategories(items) {
    // Find categories that has no more stocks.
    // The hot categories must be unique; no repeating categories.

    const hotCategories = new Set();

        for (let hotItems = 0; hotItems < items.length; hotItems++) {
        
        // If the item's stocks is zero, add its category to the hot categories set.
        
        if (items[hotItems].stocks === 0) {
            hotCategories.add(items[hotItems].category);
        }
    };
  
    // Convert the set to an array and return it.
    return Array.from(hotCategories);

};

        // The passed items array from the test are the following:
        // { id: 'tltry001', name: 'soap', stocks: 14, category: 'toiletries' },
        // { id: 'tltry002', name: 'shampoo', stocks: 8, category: 'toiletries' },
        // { id: 'tltry003', name: 'tissues', stocks: 0, category: 'toiletries' },
        // { id: 'gdgt001', name: 'phone', stocks: 0, category: 'gadgets' },
        // { id: 'gdgt002', name: 'monitor', stocks: 0, category: 'gadgets' }


 
    // The expected output after processing the items array is ['toiletries', 'gadgets'].
    // Only putting return ['toiletries', 'gadgets'] will not be counted as a passing test during manual checking of codes.


const candidateA = ['LIWf1l', 'V2hjZH', 'rDmZns', 'PvaRBI', 'i7Xw6C', 'NPhm2m'];
const candidateB = ['kcUtuu', 'LLeUTl', 'r04Zsl', '84EqYo', 'V2hjZH', 'LIWf1l'];
const result = findFlyingVoters(candidateA, candidateB);

    
function findFlyingVoters(candidateA, candidateB) {
    // Find voters who voted for both candidate A and candidate B.

    const flyingVoters = new Set();
  
    // Iterate through each voter in candidate A's array.
    
    for (let voter = 0; voter < candidateA.length; voter++) {
        // If the voter also voted for candidate B, add them to the flyingVoters set.
        
        if (candidateB.includes(candidateA[voter])) {
            flyingVoters.add(candidateA[voter]);
        }
    }
  
    // Convert the set to an array and return it.
    return Array.from(flyingVoters);
}

    // The passed values from the test are the following:




    // candidateA: ['LIWf1l', 'V2hjZH', 'rDmZns', 'PvaRBI', 'i7Xw6C', 'NPhm2m']
    // candidateB: ['kcUtuu', 'LLeUTl', 'r04Zsl', '84EqYo', 'V2hjZH', 'LIWf1l']

    // The expected output after processing the candidates array is ['LIWf1l', 'V2hjZH'].
    // Only putting return ['LIWf1l', 'V2hjZH'] will not be counted as a passing test during manual checking of codes.
    


module.exports = {
    countLetter,
    isIsogram,
    purchase,
    findHotCategories,
    findFlyingVoters
};